package be.janolaerts.jsp.address;

public interface AddressService {
    void registerAddress(AddressBean addressBean);
}