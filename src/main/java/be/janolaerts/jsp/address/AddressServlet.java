package be.janolaerts.jsp.address;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/Address")
public class AddressServlet extends HttpServlet {

    private AddressServiceImpl addressServiceImpl;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String page = "AddressForm.jsp";

        HttpSession session = req.getSession();
        AddressBean addressBean = (AddressBean) session.getAttribute("addressBean");
        if(addressBean != null) {
            page = "AddressResult.jsp";
        }

        addressServiceImpl = (AddressServiceImpl) getServletContext().getAttribute(ApplicationContextListener.ADDRESS_SERVICE);

        RequestDispatcher disp = req.getRequestDispatcher("/WEB-INF/pages/" + page);
        disp.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Reading parameters
        String lastName = req.getParameter("lastname");
        String firstName = req.getParameter("firstname");
        String street = req.getParameter("street");
        String bus = req.getParameter("bus");
        String zipCode = req.getParameter("zipcode");
        String community = req.getParameter("community");
        String country = req.getParameter("country");
        String phoneNumber = req.getParameter("phonenumber");
        String emailAddress = req.getParameter("emailaddress");

        // Making bean object
        AddressBean addressBean = new AddressBean(lastName, firstName, street, bus,
                Integer.parseInt(zipCode), community, country, phoneNumber, emailAddress);

        // Printing AddressBean object
        addressServiceImpl.registerAddress(addressBean);

        // Putting bean into session
        HttpSession session = req.getSession();
        session.setAttribute("addressBean", addressBean);

        // Putting bean into attributes
        req.setAttribute("addressBean", addressBean);

        // Forwarding to other page
        RequestDispatcher disp = req.getRequestDispatcher("/WEB-INF/pages/AddressResult.jsp");
        disp.forward(req, resp);
    }
}
