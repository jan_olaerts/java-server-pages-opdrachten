package be.janolaerts.jsp.address;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class ApplicationContextListener implements HttpSessionListener, ServletContextListener {
    public static final String ADDRESS_SERVICE = "addressService";
    AddressServiceImpl addressService;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        addressService = new AddressServiceImpl();
        sce.getServletContext().setAttribute(ADDRESS_SERVICE, addressService);
    }
}