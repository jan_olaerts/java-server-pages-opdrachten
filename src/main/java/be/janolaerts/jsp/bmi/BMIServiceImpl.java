package be.janolaerts.jsp.bmi;

public class BMIServiceImpl implements BMIService {

    @Override
    public float calculateBMI(float height, float weight) {
        return weight / (height * height);
    }
}
