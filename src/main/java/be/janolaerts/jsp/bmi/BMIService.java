package be.janolaerts.jsp.bmi;

public interface BMIService {
    float calculateBMI(float height, float weight);
}