package be.janolaerts.jsp.bmi;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/BMI")
public class BMIServlet extends HttpServlet {

    private BMIServiceImpl bmiServiceImpl = new BMIServiceImpl();
    private Cookie heightCookie;
    private Cookie weightCookie;
    private String heightError = "";
    private String weightError = "";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        float height = 0;
        float weight = 0;

        // Handling cookies
        Cookie[] cookies = req.getCookies();
        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if(cookie.getName().equals("height")) {
                    height = Float.parseFloat(cookie.getValue());
                    continue;
                }
                if(cookie.getName().equals("weight")) {
                    weight = Float.parseFloat(cookie.getValue());
                }
            }
            float BMI = bmiServiceImpl.calculateBMI(height, weight);
            req.setAttribute("BMI", String.format("%.2f", BMI));
        }

        RequestDispatcher disp = req.getRequestDispatcher("/WEB-INF/pages/BMI.jsp");
        disp.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        float height = Float.parseFloat(req.getParameter("height"));
        float weight = Float.parseFloat(req.getParameter("weight"));

        heightCookie = new Cookie("height", Float.toString(height));
        weightCookie = new Cookie("weight", Float.toString(weight));
        resp.addCookie(heightCookie);
        resp.addCookie(weightCookie);

        float BMI = bmiServiceImpl.calculateBMI(height, weight);
        req.setAttribute("BMI", String.format("%.2f", BMI));

        RequestDispatcher disp = req.getRequestDispatcher("/WEB-INF/pages/BMI.jsp");
        disp.forward(req, resp);
    }
}