<%--
  Created by IntelliJ IDEA.
  User: Bautista Olaerts
  Date: 07-Jul-20
  Time: 10:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Address Form</title>
</head>
<body>
    <form action="Address" method="POST">

        <input type="text" name="lastname" placeholder="Last name:" />
        <br/>
        <input type="text" name="firstname" placeholder="First name:" />
        <br/>
        <input type="text" name="street" placeholder="Street:" />
        <br/>
        <input type="text" name="bus" placeholder="bus" />
        <br/>
        <input type="number" name="zipcode" placeholder="Zipcode:" />
        <br/>
        <input type="text" name="community" placeholder="Community:" />
        <br/>
        <input type="text" name="country" placeholder="Country:" />
        <br/>
        <input type="text" name="phonenumber" placeholder="Phone number:" />
        <br/>
        <input type="text" name="emailaddress" placeholder="Email address:" />
        <br/>
        <input type="submit" value="Send">

</form>
</body>
</html>
