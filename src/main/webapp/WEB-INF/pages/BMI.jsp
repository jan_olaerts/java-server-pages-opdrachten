<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>BMI Calculator</title>
</head>
<body>

    <%
        float height = 0;
        float weight = 0;

        Cookie[] cookies = request.getCookies();
        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if(cookie.getName().equals("height")) {
                    height = Float.parseFloat(cookie.getValue());
                    continue;
                }
                if(cookie.getName().equals("weight")) {
                    weight = Float.parseFloat(cookie.getValue());
                }
            }
        }
    %>

    <h1>BMI Calculator</h1>

    <form method="POST">
        Your height (m): <input type="number" step="0.01" name="height" value="${ cookie.height.value }" />
        <%= height < 0 ? "Invalid value" : "" %> <br/>

        Your weight (kg): <input type="number" step="0.01" name="weight" value="${ cookie.weight.value }" />
        <%= weight < 0 ? "Invalid value" : "" %> <br/>

        <input type="submit" value="send"><br/>
    </form>

    <h2 style="${ BMI > 25.00 ? 'color:red' : 'color:green' }">Your BMI is ${ BMI }</h2>

</body>
</html>